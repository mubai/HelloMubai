package hello.controller;

import hello.service.StudentService;
import hello.domain.Student;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
public class StudentController {

	@Autowired
	private StudentService studentService;

    @Autowired
    private VelocityEngine engine;

	@RequestMapping("/students")
	@ResponseBody
	public List<Student> getAllStudentInfo() {
		List<Student> studentList = studentService.getAllStudentInfo();
		return studentList;
	}

	@RequestMapping("/student")
    @ResponseBody
	public String getStudent() {
		Template template = engine.getTemplate("student.vm");

		VelocityContext context = new VelocityContext();
		context.put("student", studentService.getStudent());

		StringWriter writer = new StringWriter();
		template.merge(context, writer);

		return writer.toString();
	}

    @RequestMapping("/web")
    public String web(Map<String, Object> model) {
        model.put("time", new Date());
        model.put("message", "Hello World!");
        return "web";
    }
}