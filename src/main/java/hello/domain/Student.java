package hello.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * Created by 白 on 2016/8/15.
 */
@Data
public class Student implements Serializable {

    private int id;
    private String name;
    private Integer age;
    private String password;
}
