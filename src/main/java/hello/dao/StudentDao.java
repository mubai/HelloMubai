package hello.dao;

import hello.domain.Student;

import java.util.List;

public interface StudentDao {
	public List<Student> queryStudentAll();
	public Student queryStudent();
}
