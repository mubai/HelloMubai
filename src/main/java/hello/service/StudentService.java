package hello.service;

import hello.dao.StudentDao;
import hello.domain.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

	@Autowired
	private StudentDao studentDao;

	public List<Student> getAllStudentInfo() {
		return studentDao.queryStudentAll();
	}

	public Student getStudent() {
		return studentDao.queryStudent();
	}
}
