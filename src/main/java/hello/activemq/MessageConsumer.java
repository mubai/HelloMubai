package hello.activemq;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class MessageConsumer {

    @JmsListener(destination = "test.queue")
    public void receiveQueue(String text) {

       System.out.println("activemq received: " + text);
    }
}