package hello;

import hello.activemq.MessageProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by 白 on 2016/8/15.
 */
@Component
public class ScheduledTasks {

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    private MessageProducer messageProducer;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Scheduled(fixedRate = 5000)
    public void reportCurrentTime() {
        System.out.println("The time is now " + dateFormat.format(new Date()));
    }

    @Scheduled(fixedRate = 5000)
    public void sendMessageToActiveMQ() {
        messageProducer.send("Hello from ActiveMQ!");
    }

    @Scheduled(fixedRate = 5000)
    public void sendMessageToRedis() {
        stringRedisTemplate.convertAndSend("chat", "Hello from Redis!");
    }
}